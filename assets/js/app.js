autosize(document.querySelectorAll('textarea'));
const postTypes = document.querySelectorAll('.post-type-item');
const selectedType = document.querySelectorAll('.selected-type');
const bookmarkList = document.querySelectorAll('.bookmark');
const likeBtnList = document.querySelectorAll('.likeBtn');
const commentElements = document.querySelectorAll('.commentDiv');
const postContainers = document.querySelectorAll('.post-container');
const paragraphs = document.querySelectorAll('.short-par');

bookmarkList.forEach(bookmark => {
    bookmark.addEventListener('click', function() {
        bookmark.classList.toggle("bookmarked")
    });
});

likeBtnList.forEach(like => {
    like.addEventListener('click', function() {
        like.classList.toggle("liked")
    });
});

postTypes.forEach(type => {
    type.addEventListener('click', function(event) {
        event.currentTarget.parentElement.parentElement.childNodes[1].textContent = event.currentTarget.textContent;
    });
})

postContainers.forEach(post => {
    post.addEventListener('click', function(event) {
        if (event.target.className.includes("commentDiv") || event.target.className.includes("commentBtn")) {
            this.querySelector('.comment-section').classList.toggle('hide-content');
        };
    });
});

paragraphs.forEach(par => {
    par.textContent.length >= 100 ? par.innerHTML = par.textContent.slice(0, 100) + ' ... <span class="readmore">Baca selengkapnya</span>' : par.innerHTML = par.textContent;
})